package service;

import dao.OrderDao;
import model.Payment;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.LinkedList;
import java.util.List;

@Service
public class PaymentsService {
    private final OrderDao dao;

    public PaymentsService(OrderDao dao) {
        this.dao = dao;
    }

    public List<Payment> generatePayments(Long id, LocalDate start, LocalDate end) {
        Integer totalAmountLeft = dao.getOrderById(id).calculateTotalPrice();

        if (totalAmountLeft <= 3) {
            throw new RuntimeException("Total price too small: " + totalAmountLeft);
        }

        List<Payment> paymentList = new LinkedList<>();

        int noOfPaymentDates = Period.between(start, end).getMonths() + 2;

        int paymentAmount = Math.max((int) Math.floor(totalAmountLeft / noOfPaymentDates), 3);

        //First payment
        paymentList.add(new Payment(paymentAmount, start.toString()));
        totalAmountLeft -= 3;

        //Generate payments for all dates if amount is big enough.
        for (int i = 1; i < noOfPaymentDates - 1; i++) {
            if (totalAmountLeft >= 3) {
                LocalDate paymentDate = LocalDate.of(
                        start.getYear(),
                        start.getMonth().plus(i),
                        1);
                paymentList.add(new Payment(paymentAmount, paymentDate.toString()));
                totalAmountLeft -= 3;
            }
        }

        //Last payment
        if (totalAmountLeft > 0) {
            int lastPaymentAmount = Math.min(totalAmountLeft, 3);
            paymentList.add(
                    new Payment(lastPaymentAmount,
                            LocalDate.of(end.getYear(),
                                    end.getMonth(),
                                    1).toString()));
            totalAmountLeft -= lastPaymentAmount;
        }

        //If some amount is still left, divide by last payments.
        if (totalAmountLeft > 0) {
            for (int i = 1; i <= totalAmountLeft; i++) {
                Payment tmpPayment = paymentList.get(paymentList.size() - i);
                tmpPayment.addToAmount(1);
            }
        }

        return paymentList;
    }

}
