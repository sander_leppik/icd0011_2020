package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Embeddable
public class OrderRow {

    @Column(name = "item_name")
    public String itemName;

    @NotNull
    @Min(1)
    public Integer quantity;

    @NotNull
    @Min(1)
    public Integer price;

}
