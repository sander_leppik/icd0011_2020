package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment {
    private int amount;
    private String date;

    public void addToAmount(int amountToAdd) {
        this.amount += amountToAdd;
    }
}

