package model;

import lombok.*;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "orders")
public class Order extends BaseEntity {

    @NonNull
    @Column(name = "order_number")
    private String orderNumber;

    @Valid
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "order_rows",
            joinColumns = @JoinColumn(name = "orders_id",
                    referencedColumnName = "id"))
    private List<OrderRow> orderRows = new ArrayList<>();

    public void addOrderRow(OrderRow orderRow) {
        orderRows.add(orderRow);
    }

    public Integer calculateTotalPrice() {
        Integer price = 0;
        for (OrderRow orderRow : orderRows) {
            price += orderRow.getPrice() * orderRow.getQuantity();
        }
        return price;
    }

}
