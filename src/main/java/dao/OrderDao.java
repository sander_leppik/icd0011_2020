package dao;

import model.Order;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;


@Repository
public class OrderDao {

    @PersistenceContext
    private EntityManager em;

    public Order getOrderById(Long id) {
        TypedQuery<Order> query = em.createQuery(
                "select o from Order o where o.id = :id",
                Order.class);

        query.setParameter("id", id);
        return query.getSingleResult();
    }

    public List<Order> getAllOrders() {
        return em.createQuery("select o from Order o")
                .getResultList();
    }

    @Transactional
    public Order insertOrder(Order order) {
        if (order.getId() == null) {
            em.persist(order);
        } else {
            em.merge(order);
        }
        em.flush();
        return order;
    }

    @Transactional
    public void deleteOrder(Long id) {
        em.remove(em.find(Order.class, id));
    }

//    public void insertOrderRows(Order order) {
//        String sql = "INSERT INTO orderrows (order_id, item_name, quantity, price) " +
//                " VALUES (?, ?, ?, ?) ";
//
//        template.batchUpdate(sql, new BatchPreparedStatementSetter() {
//            @Override
//            public void setValues(PreparedStatement ps, int i) throws SQLException {
//                OrderRow row = order.getOrderRows().get(i);
//                ps.setLong(1, order.getId());
//                ps.setString(2, row.getItemName());
//                ps.setInt(3, row.getQuantity());
//                ps.setInt(4, row.getPrice());
//            }
//
//            @Override
//            public int getBatchSize() {
//                return order.getOrderRows().size();
//            }
//        });
//    }
//
//    private static class OrdersMapper implements ResultSetExtractor<ArrayList<Order>> {
//
//        @Override
//        public ArrayList<Order> extractData(ResultSet rs) throws SQLException {
//            Map<Long, Order> orderMap = new HashMap<>();
//
//            while (rs.next()) {
//                Long orderId = rs.getLong(ORDERID.toString());
//                if (!orderMap.containsKey(orderId)) {
//                    Order order = new Order();
//                    order.setId(rs.getLong(ORDERID.toString()));
//                    order.setOrderNumber(rs.getString(ORDERNUMBER.toString()));
//                    orderMap.put(order.getId(), order);
//                }
//                Order order = orderMap.get(orderId);
//
//                OrderRow orderRow = new OrderRow();
//                orderRow.setRowId(rs.getLong(ROWID.toString()));
//                orderRow.setItemName(rs.getString(ITEMNAME.toString()));
//                orderRow.setQuantity(rs.getInt(QUANTITY.toString()));
//                orderRow.setPrice(rs.getInt(PRICE.toString()));
//
//                order.addOrderRow(orderRow);
//            }
//
//            return new ArrayList(orderMap.values());
//        }
//    }
//
//    private static class OrderMapper implements ResultSetExtractor<Order> {
//
//        @Override
//        public Order extractData(ResultSet rs) throws SQLException {
//            Order order = new Order();
//
//            while (rs.next()) {
//                if (order.getId() == null) {
//                    order.setId(rs.getLong(ORDERID.toString()));
//                    order.setOrderNumber(rs.getString(ORDERNUMBER.toString()));
//                }
//
//                OrderRow orderRow = new OrderRow();
//                orderRow.setRowId(rs.getLong(ROWID.toString()));
//                orderRow.setItemName(rs.getString(ITEMNAME.toString()));
//                orderRow.setQuantity(rs.getInt(QUANTITY.toString()));
//                orderRow.setPrice(rs.getInt(PRICE.toString()));
//
//                order.addOrderRow(orderRow);
//            }
//
//            return order;
//        }
//    }
}
