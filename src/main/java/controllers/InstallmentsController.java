package controllers;

import dao.OrderDao;
import model.Payment;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import service.PaymentsService;

import java.time.LocalDate;
import java.util.List;

@RestController
public class InstallmentsController {
    private final OrderDao dao;

    public InstallmentsController(OrderDao dao) {
        this.dao = dao;
    }

    @GetMapping("orders/{id}/installments")
    public List<Payment> getInstallments(@PathVariable Long id,
                                         @DateTimeFormat(pattern = "yyyy-MM-dd")
                                         @RequestParam(name = "start")
                                                 LocalDate start,
                                         @DateTimeFormat(pattern = "yyyy-MM-dd")
                                         @RequestParam(name = "end")
                                                 LocalDate end) {

        return new PaymentsService(dao).generatePayments(id, start, end);
    }
}
