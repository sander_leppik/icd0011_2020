DROP TABLE IF EXISTS order_rows;
DROP TABLE IF EXISTS orders;
DROP SEQUENCE IF EXISTS seq1;

CREATE SEQUENCE seq1 AS INTEGER START WITH 1;

CREATE TABLE orders
(
    id           BIGINT NOT NULL PRIMARY KEY,
    order_number VARCHAR(255)
);

CREATE TABLE order_rows
(
    item_name VARCHAR(255),
    price     INT,
    quantity  INT,
    orders_id BIGINT,
    FOREIGN KEY (orders_id)
        REFERENCES orders (id) ON DELETE CASCADE
);



-----------OLD TABLE SCHEMA--------------
-- DROP TABLE IF EXISTS orders CASCADE;
--
-- DROP SEQUENCE IF EXISTS order_sequence CASCADE;
--
-- CREATE SEQUENCE order_sequence START WITH 1;
--
-- CREATE TABLE orders (
--    order_id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('order_sequence'),
--    order_number VARCHAR(255) NOT NULL
-- );
--
--
--
-- -- ALTER TABLE IF EXISTS orderRows DROP CONSTRAINT IF EXISTS fk_order;
--
-- DROP TABLE IF EXISTS orderRows CASCADE;
--
-- DROP SEQUENCE IF EXISTS row_sequence CASCADE;
--
-- CREATE SEQUENCE row_sequence START WITH 1;
--
-- CREATE TABLE orderRows (
--    row_id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('row_sequence'),
--    order_id BIGINT NOT NULL,
--    item_name VARCHAR(255) NOT NULL,
--    quantity INT NOT NULL,
--    price INT NOT NULL,
--    CONSTRAINT fk_order FOREIGN KEY(order_id) REFERENCES orders(order_id) ON DELETE CASCADE
-- );
